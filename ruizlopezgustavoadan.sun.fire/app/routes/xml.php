<?php

$app->get('/xml', function() use($app) {
  $app->response->headers->set('Content-Type', 'application/xml');
  $datos = array(
    'contenido_api' => array(
      array(
        'GET' => "GET",
        'tema1' => "Gambardella, Matthew",
        'tema2' => "XML Developer's Guide",
        'tema3' => "Computer",
        'indicador' => "44.95",
        'publish_date' => "2000-10-01",
        'description' => "An in-depth look at creating applications with XML."
      ),
      array(
        'id' => 'bk102',
        'author' => "Ralls, Kim",
        'title' => "Midnight Rain",
        'genre' => "Fantasy",
        'price' => "5.95",
        'publish_date' => "2000-12-16",
        'description' => "A former architect battles corporate zombies."
      )
    )
  );
  $app->render('xml.php', $datos);
});
